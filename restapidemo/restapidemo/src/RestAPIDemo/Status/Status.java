package RestAPIDemo.Status;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/status")
public class Status {

	@Path("/title")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getTitle(){
		return
		"{"
		+"	{"
		+"	\"pageId\":\"{644f9ae8-d1bd-46c7-a77d-f3b0b9e59c13}\" "
		+"	}"
		+"}";
	}
	
	@Path("/version")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getVersion(){
		return "7.0.10.3";
	}
}
